*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${TEAM_HEADER_LABEL} =  css=#team > div > div:nth-child(1) > div > h2
#${TEAM_HEADER_LABEL} = css#bs-example-navbar-collapse-1 > ul > li:nth-child(5) > a

*** Keywords ***
Verify Page Loaded
    wait until page contains element  ${TEAM_HEADER_LABEL}

Validate Page Contents
    ${ElementText} =  get text  ${TEAM_HEADER_LABEL}
    should be equal as strings  ${ElementText}  Our Amazing Team  ignore_case=true
    #element text should be  ${TEAM_HEADER_LABEL}  Our Amazing Team
    #werkt niet met Chrome wel met Edge